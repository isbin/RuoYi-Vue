import request from '@/utils/request'

export function listOrder(data) {
  return request({
    url: '/order/page',
    method: 'post',
    data
  })
}


export function orderDown(data) {
  return request({
    url: '/order/download',
    method: 'post',
    data,
    responseType: 'blob',
  })
}

